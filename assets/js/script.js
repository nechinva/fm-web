jQuery(document).ready(function ($) {
    var AUTH_TOKEN = $('#ajax-api-token').val();

    $.ajax({
        url       : $('#ajax-api-url').val() + '/lists/index',
        type      : 'GET',
        beforeSend: function (request) {
            request.setRequestHeader('auth-token', AUTH_TOKEN);
        },
        success   : function (result) {
            if (undefined !== result.data) {
                var html = '<option value="0">Select list</option>';
                $.each(result.data, function(index, list) {
                    html += '<option value="' + list.id + '">' + list.name + '</option>'
                });
                $('#item-list').html(html);
            }
        },
        error     : function (jqXHR, textStatus, errorThrown) {
            alert(JSON.parse(jqXHR.responseText).error);
        }
    });

    $('#submit-button').on('click', function() {
        var form = $('form'),
            itemEl = $('#item-name'),
            listEl = $('#item-list');

        form.removeClass('was-validated');

        if (itemEl.val() === '') {
            form.addClass('was-validated');
            itemEl.focus();
            return false;
        }

        if (listEl.val() == 0) {
            alert('Please select the list');
            return false;
        }

        $(this).attr('disabled', true);

        $.ajax({
            url       : $('#ajax-api-url').val() + '/items/create/' + listEl.val(),
            type      : 'POST',
            data      : {name: itemEl.val()},
            beforeSend: function (request) {
                request.setRequestHeader('auth-token', AUTH_TOKEN);
            },
            success   : function (result) {
                $('#submit-button').attr('disabled', false);

                if (undefined !== result.error) {
                    alert(result.error);
                } else {
                    alert('New item was successfully added');
                    $('form')[0].reset();
                }
            },
            error     : function (jqXHR, textStatus, errorThrown) {
                alert(JSON.parse(jqXHR.responseText).error);
            }
        });
    });
});
